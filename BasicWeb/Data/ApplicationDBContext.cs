﻿using BasicWeb.Models;
using Microsoft.EntityFrameworkCore;

namespace BasicWeb.Data
{
    public class ApplicationDBContext:DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options )
        {

        }
        public DbSet<Student>Students { get; set; }
    }
}
