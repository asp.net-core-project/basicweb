﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BasicWeb.Models
{
    public class Student
    {
        [Key]   
        public int Id { get; set; }

        [Required(ErrorMessage ="กรุณาป้อนชื่อสินค้า")]
        [DisplayName("ชื่อสินค้า")]
        public string Name { get; set; }

        
        [DisplayName("ราคาสินค้า")]
        [Range(5000,50000, ErrorMessage = "กรุณาป้อนราคาสินค้าระหว่าง 5000-50000")]
        public int Price { get; set; }
        
    }
}
